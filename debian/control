Source: primus-vk
Section: utils
Priority: optional
Maintainer: Debian NVIDIA Maintainers <pkg-nvidia-devel@lists.alioth.debian.org>
Uploaders:
 Luca Boccassi <bluca@debian.org>,
 Andreas Beckmann <anbe@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-bash-completion,
 libvulkan-dev (>= 1.1.108),
 libwayland-dev,
 libx11-dev,
 mesa-common-dev,
 libxcb1-dev,
 libxrandr-dev,
Standards-Version: 4.7.0
Homepage: https://github.com/felixdoerre/primus_vk
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/nvidia-team/primus-vk
Vcs-Git: https://salsa.debian.org/nvidia-team/primus-vk.git

Package: primus-vk
Architecture: all
Depends:
 primus,
 libprimus-vk1,
 ${misc:Depends}
Suggests:
 primus-vk-nvidia,
Breaks:
 primus-vk-nvidia (<< 1.4-2~),
Replaces:
 primus-vk-nvidia (<< 1.4-2~),
Description: Vulkan layer for GPU offloading
 Typically you want to display an image rendered on a more powerful
 GPU on a display managed by an internal GPU. The layer in this package will
 direct rendering commands to a dedicated, more powerful GPU an when such an
 image is displayed it will be copied to the integrated CPU for displaying.
 .
 The user can activate offloading for individual applications by launching
 them with the "pvkrun" program.

Package: primus-vk-nvidia
Section: contrib/utils
Architecture: amd64 i386 arm64 ppc64el
Depends:
 primus-vk (= ${source:Version}),
 nvidia-primus-vk-wrapper,
 ${misc:Depends}
Conflicts:
 primus-vk-nvidia-i386,
Description: NVIDIA Optimus support for Vulkan applications
 This metapackage depends on all packages necessary to run
 OpenGL and Vulkan applications on a dedicated graphics card
 on systems with Nvidia + Intel Optimus hardware.

Package: libprimus-vk1
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 libvulkan1 (>= 1.1.108),
 ${shlibs:Depends},
 ${misc:Depends}
Recommends:
 libprimus-vk1:i386 (= ${binary:Version}) [amd64],
Breaks:
 primus-vk (<< 1.4-2~),
Replaces:
 primus-vk (<< 1.4-2~),
Description: vulkan layer library for the primus-vk layer
 This library implements the primus-vk layer. It decides which GPU
 the individual Vulkan API calls should go against and does the image
 copying.

Package: nvidia-primus-vk-common
Section: contrib/libs
Architecture: amd64 i386 arm64 ppc64el
Multi-Arch: foreign
Depends:
 ${misc:Depends}
Recommends:
 nvidia-primus-vk-wrapper,
Breaks:
 primus-vk-nvidia (<< 1.4-2~),
Replaces:
 primus-vk-nvidia (<< 1.4-2~),
Description: thin wrapper for the NVIDIA binary Vulkan ICD (common files)
 This package disables the original NVIDIA binary Vulkan driver and registers
 the thin wrapper from nvidia-primus-vk-wrapper to ensure that the NVIDIA
 driver is usable with bumblebee.

Package: nvidia-primus-vk-wrapper
Section: contrib/libs
Architecture: amd64 i386 arm64 ppc64el
Multi-Arch: same
Depends:
 nvidia-primus-vk-common,
 libvulkan1,
 ${nvidia:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Recommends:
 nvidia-primus-vk-wrapper:i386 (= ${binary:Version}) [amd64],
Breaks:
 libnv-vulkan-wrapper1 (>= 0),
Replaces:
 libnv-vulkan-wrapper1 (>= 0),
Description: thin wrapper for using the NVIDIA binary Vulkan ICD with bumblebee
 This library is a thin wrapper around the Vulkan API functions of the
 nvidia Vulkan driver. The Vulkan driver misbehaves for example by reading the
 DISPLAY environment variable and connecting to that display server. This
 library works around that misbehaviour, so the driver can be used with
 bumblebee.
