primus-vk (1.6.4-3) unstable; urgency=medium

  * Update the list of supported drivers.
  * Bump Standards-Version to 4.7.0, no changes needed.

 -- Andreas Beckmann <anbe@debian.org>  Thu, 14 Nov 2024 09:47:49 +0100

primus-vk (1.6.4-2) unstable; urgency=medium

  * Update the list of supported drivers.

 -- Andreas Beckmann <anbe@debian.org>  Sat, 27 Jan 2024 00:30:41 +0100

primus-vk (1.6.4-1) unstable; urgency=medium

  [ Felix Dörre ]
  * New upstream release.
    - remove dependency on vulkan-validationlayers, this allows compiling
    with vulkan >=1.3.268
    - link to Gentoo overlay from the Readme

 -- Andreas Beckmann <anbe@debian.org>  Tue, 12 Dec 2023 10:34:12 +0100

primus-vk (1.6.3-1) unstable; urgency=medium

  [ Felix Dörre ]
  * New upstream release.
    Resolved build compatibility with newer vulkan-validationlayers
    (Closes: #1042122)

 -- Andreas Beckmann <anbe@debian.org>  Mon, 31 Jul 2023 14:49:27 +0200

primus-vk (1.6.2-2) unstable; urgency=medium

  * Switch to dh-sequence-bash-completion.
  * Update the list of supported drivers.
  * Bump Standards-Version to 4.6.2, no changes needed.

 -- Andreas Beckmann <anbe@debian.org>  Thu, 05 Jan 2023 10:37:29 +0100

primus-vk (1.6.2-1) unstable; urgency=medium

  [ Felix Dörre ]
  * New upstream release.
    Fixes compatibility with newer wine versions using a specific vulkan
    extension.

  [ Andreas Beckmann ]
  * Update the list of supported Tesla drivers.
  * Bump Standards-Version to 4.6.1, no changes needed.

 -- Andreas Beckmann <anbe@debian.org>  Thu, 07 Jul 2022 20:52:56 +0200

primus-vk (1.6.1-2) unstable; urgency=medium

  * Update the list of supported Tesla drivers.
  * Bump Standards-Version to 4.6.0, no changes needed.

 -- Andreas Beckmann <anbe@debian.org>  Sun, 03 Apr 2022 23:30:40 +0200

primus-vk (1.6.1-1) unstable; urgency=medium

  [ Felix Dörre ]
  * New upstream release.

 -- Luca Boccassi <bluca@debian.org>  Sat, 26 Dec 2020 12:21:12 +0000

primus-vk (1.5-2) unstable; urgency=medium

  * Build nvidia-specific packages for arm64.

 -- Andreas Beckmann <anbe@debian.org>  Fri, 24 Jul 2020 10:37:20 +0200

primus-vk (1.5-1) unstable; urgency=medium

  * New upstream release.

 -- Andreas Beckmann <anbe@debian.org>  Thu, 11 Jun 2020 15:00:43 +0200

primus-vk (1.4-2) unstable; urgency=medium

  * Sync with upstream master.
  * Add upstream metadata.
  * Use dh_bash-completion.
  * Use dh_auto_install.
  * Switch to debhelper-compat (= 13).
  * Bump Standards-Version to 4.5.0, no changes needed.
  * Add myself to Uploaders.
  * Move implicit_layer.d/primus_vk.json into libprimus-vk1.
  * Use 'Recommends: libprimus-vk1:i386 [amd64]' to pull in the 32-bit parts.
  * Support all NVIDIA drivers with Vulkan ICD.  (Closes: #948952)
  * Move the NVIDIA Vulkan ICD wrapper from libnv-vulkan-wrapper1 to
    nvidia-primus-vk-{common,wrapper}.
  * nvidia-primus-vk-wrapper: Try libGLX_nvidia.so.0 first.
  * Move pvkrun back from primus-vk-nvidia to primus-vk.
  * Make primus-vk-nvidia an arch-specific metapackage.

 -- Andreas Beckmann <anbe@debian.org>  Wed, 20 May 2020 20:14:08 +0200

primus-vk (1.4-1) unstable; urgency=medium

  [ Felix Dörre ]
  * Upstream fixes which ensure compatibility with 440-nvidia-driver

 -- Luca Boccassi <bluca@debian.org>  Mon, 10 Feb 2020 18:26:45 +0000

primus-vk (1.3-3) unstable; urgency=medium

  * No change source upload to allow migration to testing.

 -- Luca Boccassi <bluca@debian.org>  Wed, 20 Nov 2019 10:01:00 +0000

primus-vk (1.3-2) unstable; urgency=medium

  [ Felix Dörre ]
  * restrict architectures for libnv-vulkan-wrapper1 to those that build
    NVIDIA drivers and the required libraries
  * another section-fix: move primus-vk from contrib/utils to utils

 -- Luca Boccassi <bluca@debian.org>  Thu, 14 Nov 2019 18:20:18 +0000

primus-vk (1.3-1) unstable; urgency=medium

  [ Felix Dörre ]
  * New upstream release v1.3 with a few improvements
  * move primus-vk-nvidia-i386 to correct section

 -- Luca Boccassi <bluca@debian.org>  Tue, 12 Nov 2019 10:20:10 +0000

primus-vk (1.2-1) unstable; urgency=medium

  [ Felix Dörre ]
  * Initial release (Closes: #940631)

 -- Luca Boccassi <bluca@debian.org>  Sun, 06 Oct 2019 20:24:34 +0100
